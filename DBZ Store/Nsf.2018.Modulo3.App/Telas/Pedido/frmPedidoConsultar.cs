﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.PedidoItem;
using Nsf._2018.Modulo3.App.DB.Pedido;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoConsultar : UserControl
    {
        public frmPedidoConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PedidoBusiness business = new PedidoBusiness();
            List<PedidoConsultarView> lista = business.Consultar(txtCliente.Text);

            dgvPedidos.AutoGenerateColumns = false;
            dgvPedidos.DataSource = lista;
        }
    }
}
