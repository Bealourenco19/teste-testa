﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoCadastrar : UserControl
    {
        public frmProdutoCadastrar()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            ProdutoDTO produto = new ProdutoDTO();
            produto.Produto = txtProduto.Text;
            produto.Preço = Convert.ToDecimal(txtPreco.Text);
           
            ProdutoBusiness business = new ProdutoBusiness();
            business.Salvar(produto);

            MessageBox.Show("Produto salvo com sucesso.");

        }
    }
}
