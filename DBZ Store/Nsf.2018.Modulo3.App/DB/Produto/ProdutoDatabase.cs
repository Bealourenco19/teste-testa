﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    public class ProdutoDatabase
    {
        public int Salvar (ProdutoDTO produto)
        {
            string script = @"insert into tb_produto (  nm_produto, vl_preco) values (@nm_produto, @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", produto.Produto));
            parms.Add(new MySqlParameter("vl_preco", produto.Preço));
            Database db = new Database();
             
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        
        public  List<ProdutoDTO> Consultar( string produtos)
        {
            string script =
            @"SELECT * 
                FROM tb_produto 
               WHERE nm_produto like @nm_produto ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + produtos + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> prod = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO novoProduto = new ProdutoDTO();
                novoProduto.Id = reader.GetInt32("id_produto");
                novoProduto.Produto = reader.GetString("nm_produto");
                novoProduto.Preço = reader.GetInt32("vl_preco");
                prod.Add(novoProduto);
            }
            reader.Close();

            return prod;
        }

        public List<ProdutoDTO> Listar()
        {
            string script =
            @"SELECT * 
                FROM tb_produto 
";

            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> prod = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO novoProduto = new ProdutoDTO();
                novoProduto.Id = reader.GetInt32("id_produto");
                novoProduto.Produto = reader.GetString("nm_produto");
                novoProduto.Preço = reader.GetInt32("vl_preco");


                prod.Add(novoProduto);
            }
            reader.Close();

            return prod;
        }

    }
}
