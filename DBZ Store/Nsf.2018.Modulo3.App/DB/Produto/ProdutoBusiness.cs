﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    public class ProdutoBusiness
    {
        ProdutoDatabase db = new ProdutoDatabase();

         public int Salvar(ProdutoDTO produto)
        {
            if (produto.Produto == string.Empty)
            {
                throw new ArgumentException("Produto é obrigatório.");
            }
            if (produto.Preço == 0)
            {
                throw new ArgumentException("Preço é obrigatório.");
            }

            return db.Salvar(produto);
        }

       
        public List<ProdutoDTO> Consultar(string produtos)
        {
            
            return db.Consultar(produtos);
             
        }

        public List<ProdutoDTO> Listar()
        {
           return db.Listar();
        }
    }
}
