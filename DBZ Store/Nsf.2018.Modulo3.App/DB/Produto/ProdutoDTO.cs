﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    public class ProdutoDTO
    {
        public int Id { get; set; }
        public string Produto { get; set; }
        public decimal Preço { get; set; }
    }
}
