﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.PedidoItem;
using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produtos)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();
            foreach (ProdutoDTO item in produtos)
            {
                PedidoItemDTO itemDTO = new PedidoItemDTO();
                itemDTO.IdPedido = idPedido;
                itemDTO.IdProduto = item.Id;

                itemBusiness.Salvar(itemDTO);
            }
            return idPedido;
        }

        public List<PedidoConsultarView> Consultar(string nome)
        {
            PedidoDatabase pedidoDB = new PedidoDatabase();
            return pedidoDB.Consultar(nome);
        }

    }
}
